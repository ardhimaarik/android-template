package com.kirra.template.kirrabase.data.manager.impl;

import android.os.Environment;

import com.kirra.template.kirrabase.data.manager.DiskManager;

import java.io.File;

/******************************************************************************
 * Modified  : Ardhi Ma'arik
 * Email   : ardhimaarik2@gmail.com
 * Created : Mar 5, 2018 5:06:10 PM
 * Project : Template
 *
 * Copyright (c) 2017 "Kirra" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/

public class DiskManagerImpl implements DiskManager {

    public DiskManagerImpl() {
    }

    @Override
    public boolean isFileExist(String path) {
        File dir = new File(path);
        return dir.exists();
    }

    @Override
    public boolean isFolderExist(String path) {
        File dir = new File(path);
        return dir.exists();
    }

    @Override
    public boolean isFile(String path) {
        File dir = new File(path);
        return dir.isFile();
    }

    @Override
    public boolean isFolder(String path) {
        File dir = new File(path);
        return dir.isDirectory();
    }

    /**
     * @return true  Enough. false   Too little.
     * @brief Determines if there is enough storage space.
     */
    @Override
    public boolean isDiskFree() {
        int min_free_storagespace = 10;
        // Is the amount of free space <= min_free_storagespace% of the total space, return false. Otherwise return true;
        long freespace = Environment.getExternalStorageDirectory().getFreeSpace() / 1048576;
        long totalspace = Environment.getExternalStorageDirectory().getTotalSpace() / 1048576;
//        Log.d(TAG, "StorageSpace: " + String.valueOf((int) (((double) freespace / (double) totalspace) * 100)) + "% remaining.");
        return freespace > ((totalspace / 100) * min_free_storagespace);
    }

    /**
     * @return true  Enough. false   Too little.
     * @brief Determines if there is enough storage space.
     */
    @Override
    public boolean isDiskFree(int minPersen) {
        int min_free_storagespace = minPersen;
        // Is the amount of free space <= min_free_storagespace% of the total space, return false. Otherwise return true;
        long freespace = Environment.getExternalStorageDirectory().getFreeSpace() / 1048576;
        long totalspace = Environment.getExternalStorageDirectory().getTotalSpace() / 1048576;
//        Log.d(TAG, "StorageSpace: " + String.valueOf((int) (((double) freespace / (double) totalspace) * 100)) + "% remaining.");
        return freespace > ((totalspace / 100) * min_free_storagespace);
    }

    /**
     * @return True/false.
     * @brief Checks if external storage is available for read and write.
     */
    @Override
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    @Override
    public void makeDir(String path) {
        File dir = new File(path);
        if (!dir.exists())
            dir.mkdirs();
    }
}
