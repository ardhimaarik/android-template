package com.kirra.template.kirrabase.data.manager.impl;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.GoogleApiClient;
//import com.kirra.template.kirrabase.data.entity.UserEntity;
import com.kirra.template.kirrabase.data.manager.AuthManager;
//import com.kiraa.mobile.domain.User;

import io.reactivex.Observable;

/******************************************************************************
 * Modified  : Ardhi Ma'arik
 * Email   : ardhimaarik2@gmail.com
 * Created : Mar 5, 2018 5:06:10 PM
 * Project : Template
 *
 * Copyright (c) 2017 "Kirra" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/

public class AuthManagerImpl implements AuthManager {
    private GoogleApiClient googleApiClient;

    public AuthManagerImpl(GoogleApiClient googleApiClient) {
        this.googleApiClient = googleApiClient;
    }

//    @Override
//    public Observable<User> signInGoogle(GoogleSignInAccount acct) {
//        return null;
//    }
//
//    @Override
//    public Observable<User> signOut() {
//        return null;
//    }

    @Override
    public boolean isSignedIn() {
        return false;
    }

    @Override
    public String getCurrentUserId() {
        return null;
    }
}

//    @Provides
//    @AppScope
//    GoogleApiClient providesGoogleApiClient() {
//        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//                .requestIdToken(app.getString(R.string.default_web_client_id))
//                .requestEmail()
//                .build();
//
//        return new GoogleApiClient.Builder(app)
//                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
//                .build();
//    }
//
//    @Provides
//    @AppScope
//    AuthManager providesAuthManager(GoogleApiClient googleApiClient) {
//        return new AuthManagerImpl(googleApiClient);
//    }