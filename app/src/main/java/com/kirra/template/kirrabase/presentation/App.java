package com.kirra.template.kirrabase.presentation;

import android.app.Application;
import android.support.annotation.NonNull;

import com.kirra.template.kirrabase.presentation.injection.AppComponent;
import com.kirra.template.kirrabase.presentation.injection.AppModule;
import com.kirra.template.kirrabase.presentation.injection.DaggerAppComponent;

/******************************************************************************
 * Author  : Kirra
 * Email   : ardhimaarik2@gmail.com
 * Created : Mar 5, 2018 4:14:16 PM
 * Project : Template
 *
 * Copyright (c) 2017 "Kirra" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/

public final class App extends Application {
    private AppComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        mAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    @NonNull
    public AppComponent getAppComponent() {
        return mAppComponent;
    }
}