package com.kirra.template.kirrabase.data.exception;

/******************************************************************************
 * Modified  : Ardhi Ma'arik
 * Email   : ardhimaarik2@gmail.com
 * Created : Mar 5, 2018 4:44:16 PM
 * Project : Template
 *
 * Copyright (c) 2017 "Kirra" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/

/**
 * Exception throw by the application when a there is a network connection exception.
 */
public class NetworkConnectionException extends Exception {

    public NetworkConnectionException() {
        super();
    }

    public NetworkConnectionException(final Throwable cause) {
        super(cause);
    }
}