package com.kirra.template.kirrabase.presentation.injection;

import android.content.Context;

import com.kirra.template.kirrabase.presentation.App;

import javax.inject.Singleton;

import dagger.Component;


/******************************************************************************
 * Author  : Kirra
 * Email   : ardhimaarik2@gmail.com
 * Created : Mar 5, 2018 4:14:16 PM
 * Project : Template
 *
 * Copyright (c) 2017 "Kirra" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/

@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {
    Context getAppContext();

    App getApp();
}