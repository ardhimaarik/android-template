package com.kirra.template.kirrabase.data.manager;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
//import com.kirra.template.kirrabase.data.entity.UserEntity;
//import com.kiraa.mobile.domain.User;

import io.reactivex.Observable;

/******************************************************************************
 * Modified  : Ardhi Ma'arik
 * Email   : ardhimaarik2@gmail.com
 * Created : Mar 5, 2018 5:06:10 PM
 * Project : Template
 *
 * Copyright (c) 2017 "Kirra" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/

public interface AuthManager {
//    Observable<User> signInGoogle(GoogleSignInAccount acct);
//
//    Observable<User> signOut();

    boolean isSignedIn();

    String getCurrentUserId();
}
