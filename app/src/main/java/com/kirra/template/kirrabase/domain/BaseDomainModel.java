package com.kirra.template.kirrabase.domain;

/******************************************************************************
 * Author  : Ardhi Ma'arik
 * Email   : ardhimaarik2@gmail.com
 * Created : Mar 5, 2018 4:33:13 PM
 * Project : Template
 *
 * Copyright (c) 2017 "Kirra" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/

public class BaseDomainModel {
    private final String id;
    private String created;
    private String modified;

    public BaseDomainModel(String id) {
        this.id = id;
    }

    public String getid() {
        return id;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }
}