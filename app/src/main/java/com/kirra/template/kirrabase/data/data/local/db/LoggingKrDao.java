package com.kirra.template.kirrabase.data.data.local.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.kirra.template.kirrabase.data.entity.LoggingKrEntity;

import java.util.List;

import io.reactivex.Flowable;

/******************************************************************************
 * Author  : Ardhi Maarik
 * Email   : ardhimaarik2@gmail.com
 * Created : 05/03/18
 * Project : BK_Project
 *
 * Copyright (c) 2017 "PT Bahasa Kinerja Utama" - bahasakita.co.id
 * All Rights Reserved.
 ******************************************************************************/
@Dao
public interface LoggingKrDao {
    @Query("SELECT * FROM loggingkr")
    Flowable<List<LoggingKrEntity>> getAllList();

    @Query("SELECT * FROM loggingkr WHERE id = :id")
    Flowable<LoggingKrEntity> getDetail(String id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void save(LoggingKrEntity entity);

    @Update
    void update(LoggingKrEntity entity);

    @Query("DELETE FROM loggingkr")
    void deleteAll();

    @Query("DELETE FROM loggingkr WHERE id = :id")
    void delete(String id);

}
