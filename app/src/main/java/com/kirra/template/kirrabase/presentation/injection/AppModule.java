package com.kirra.template.kirrabase.presentation.injection;

import android.content.Context;
import android.support.annotation.NonNull;

import com.kirra.template.kirrabase.presentation.App;

import dagger.Module;
import dagger.Provides;


/******************************************************************************
 * Author  : Kirra
 * Email   : ardhimaarik2@gmail.com
 * Created : Mar 5, 2018 4:14:16 PM
 * Project : Template
 *
 * Copyright (c) 2017 "Kirra" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/

@Module
public final class AppModule {
    @NonNull
    private final App mApp;

    public AppModule(@NonNull App app) {
        mApp = app;
    }

    @Provides
    public Context provideAppContext() {
        return mApp;
    }

    @Provides
    public App provideApp() {
        return mApp;
    }
}
