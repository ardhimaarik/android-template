package com.kirra.template.kirrabase.data.manager.impl;

import android.media.AudioRecord;

import com.kirra.template.kirrabase.data.manager.RecorderManager;

/******************************************************************************
 * Modified  : Ardhi Ma'arik
 * Email   : ardhimaarik2@gmail.com
 * Created : Mar 5, 2018 5:06:10 PM
 * Project : Template
 *
 * Copyright (c) 2017 "Kirra" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/

public class RecorderManagerImpl implements RecorderManager {
    private VoiceRecorder mVoiceRecorder;
    private VoiceRecorder.Callback mCallback;
    private boolean isRecording = false;

    public RecorderManagerImpl(VoiceRecorder.Callback mCallback) {
        this.mCallback = mCallback;
    }

    @Override
    public void start() {
        if (mVoiceRecorder != null) {
            mVoiceRecorder.stop();
        }
        mVoiceRecorder = new VoiceRecorder(mCallback);
        mVoiceRecorder.start();
        isRecording = true;
    }

    @Override
    public void stop() {
        if (mVoiceRecorder != null) {
            mVoiceRecorder.stop();
            mVoiceRecorder = null;
            isRecording = false;
        }
    }

    @Override
    public boolean isRecording() {
        return isRecording;
    }

    @Override
    public int getSampleRate() {
        if (mVoiceRecorder != null) {
            return mVoiceRecorder.getSampleRate();
        }
        return -1;
    }

    @Override
    public int getAmplitudo() {
        if (mVoiceRecorder != null) {
            return mVoiceRecorder.getAmplitudo();
        }
        return -1;
    }
}
