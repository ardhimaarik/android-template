package com.kirra.template.kirrabase.presentation.presenter.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.kirra.template.kirrabase.presentation.presenter.BasePresenter;

/******************************************************************************
 * Author  : Kirra
 * Email   : ardhimaarik2@gmail.com
 * Created : Mar 5, 2018 4:14:16 PM
 * Project : Template
 *
 * Copyright (c) 2017 "Kirra" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/

/**
 * Abstract presenter implementation that contains base implementation for other presenters.
 * Subclasses must call super for all {@link BasePresenter} method overriding.
 */
public abstract class BasePresenterImpl<V> implements BasePresenter<V> {
    /**
     * The view
     */
    @Nullable
    protected V mView;

    @Override
    public void onViewAttached(@NonNull V view) {
        mView = view;
    }


    @Override
    public void onStart(boolean viewCreated) {

    }

    @Override
    public void onStop() {

    }


    @Override
    public void onViewDetached() {
        mView = null;
    }

    @Override
    public void onPresenterDestroyed() {

    }
}
