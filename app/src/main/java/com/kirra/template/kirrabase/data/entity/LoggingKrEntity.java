package com.kirra.template.kirrabase.data.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import org.joda.time.DateTime;

/******************************************************************************
 * Author  : Ardhi Maarik
 * Email   : ardhimaarik2@gmail.com
 * Created : 05/03/18
 * Project : BK_Project
 *
 * Copyright (c) 2017 "PT Bahasa Kinerja Utama" - bahasakita.co.id
 * All Rights Reserved.
 ******************************************************************************/
@Entity(tableName = "loggingkr")
public class LoggingKrEntity {
    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "id")
    private String id;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "timeStamp")
    private DateTime timeStamp;

    public LoggingKrEntity() {
    }

    @Ignore
    public LoggingKrEntity(@NonNull String id, String name, DateTime timeStamp) {
        this.id = id;
        this.name = name;
        this.timeStamp = timeStamp;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DateTime getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(DateTime timeStamp) {
        this.timeStamp = timeStamp;
    }
}
