package com.kirra.template.kirrabase.data.manager;

/******************************************************************************
 * Modified  : Ardhi Ma'arik
 * Email   : ardhimaarik2@gmail.com
 * Created : Mar 5, 2018 5:06:10 PM
 * Project : Template
 *
 * Copyright (c) 2017 "Kirra" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/

public interface DiskManager {
    boolean isFileExist(String path);

    boolean isFolderExist(String path);

    boolean isFile(String path);

    boolean isFolder(String path);

    boolean isDiskFree();

    boolean isDiskFree(int minPersen);

    boolean isExternalStorageWritable();

    void makeDir(String path);
}
