package com.kirra.template.kirrabase.data.exception;

/******************************************************************************
 * Modified  : Ardhi Ma'arik
 * Email   : ardhimaarik2@gmail.com
 * Created : Mar 5, 2018 4:44:16 PM
 * Project : Template
 *
 * Copyright (c) 2017 "Kirra" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/

/**
 * Exception throw by the application when a Data search can't return a valid result.
 */
public class DataNotFoundException extends Exception {
    public DataNotFoundException() {
        super();
    }
}