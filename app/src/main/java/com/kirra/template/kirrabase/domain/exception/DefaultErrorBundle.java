package com.kirra.template.kirrabase.domain.exception;

/******************************************************************************
 * Modified  : Ardhi Ma'arik
 * Email   : ardhimaarik2@gmail.com
 * Created : Mar 5, 2018 4:33:13 PM
 * Project : Template
 *
 * Copyright (c) 2017 "Kirra" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/


public class DefaultErrorBundle implements ErrorBundle {
    private static final String DEFAULT_ERROR_MSG = "Unknown error";

    private final Exception exception;

    public DefaultErrorBundle(Exception exception) {
        this.exception = exception;
    }

    @Override
    public Exception getException() {
        return exception;
    }

    @Override
    public String getErrorMessage() {
        return (exception != null) ? this.exception.getMessage() : DEFAULT_ERROR_MSG;
    }
}
