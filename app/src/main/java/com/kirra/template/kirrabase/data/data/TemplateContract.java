package com.kirra.template.kirrabase.data.data;

import android.provider.BaseColumns;

/******************************************************************************
 * Modified  : Ardhi Ma'arik
 * Email   : ardhimaarik2@gmail.com
 * Created : Mar 5, 2018 4:44:16 PM
 * Project : Template
 *
 * Copyright (c) 2017 "Kirra" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/

public class TemplateContract {

    /* Inner class that defines the table contents of the user table */
    public static final class UserEntry implements BaseColumns {

        // Table name
        public static final String TABLE_NAME = "user";

        public static final String COLUMN_ID = "id_user";

        public static final String COLUMN_USERNAME = "username";

        public static final String COLUMN_EMAIL = "email";

        public static final String COLUMN_PHOTO = "photo";

    }

    //TODO-data-contract: TemplateContract create class contract for table

}
