package com.kirra.template.kirrabase.data.data.local.db;

import android.arch.persistence.room.TypeConverter;

import org.joda.time.DateTime;

/******************************************************************************
 * Author  : Ardhi Maarik
 * Email   : ardhimaarik2@gmail.com
 * Created : 08/11/17
 * Project : BK_Project
 *
 * Copyright (c) 2017 "PT Bahasa Kinerja Utama" - bahasakita.co.id
 * All Rights Reserved.
 ******************************************************************************/
public class DateTimeConverter {
    @TypeConverter
    public static DateTime toDateTime(Long timestamp) {
        return timestamp == null ? null : new DateTime(timestamp);
    }

    @TypeConverter
    public static Long toTimestamp(DateTime date) {
        return date == null ? null : date.getMillis();
    }
}
