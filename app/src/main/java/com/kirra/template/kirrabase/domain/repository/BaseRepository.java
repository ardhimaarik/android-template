package com.kirra.template.kirrabase.domain.repository;

import com.kirra.template.kirrabase.domain.BaseDomainModel;

import java.util.List;

import io.reactivex.Observable;

/******************************************************************************
 * Author  : Ardhi Ma'arik
 * Email   : ardhimaarik2@gmail.com
 * Created : Mar 5, 2018 4:33:13 PM
 * Project : Template
 *
 * Copyright (c) 2017 "Kirra" - semangatkecil.blogspot.com
 * All Rights Reserved. 
 ******************************************************************************/

public interface BaseRepository<T extends BaseDomainModel> {
    /**
     * Get an {@link Observable} which will emit a List of {@link T}.
     */
    Observable<List<T>> getListDatas(String id);

    /**
     * Get an {@link Observable} which will emit a {@link T}.
     *
     * @param id The user id used to retrieve user data.
     */
    Observable<T> getData(String id);

    void saveData(T utterance);

    void updateData(T utterance);

    void deleteData(String id);

    void deleteAllData();
}