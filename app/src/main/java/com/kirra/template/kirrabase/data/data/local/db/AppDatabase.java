package com.kirra.template.kirrabase.data.data.local.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import com.kirra.template.kirrabase.data.entity.LoggingKrEntity;


/******************************************************************************
 * Author  : Ardhi Maarik
 * Email   : ardhimaarik2@gmail.com
 * Created : 08/11/17
 * Project : BK_Project
 *
 * Copyright (c) 2017 "PT Bahasa Kinerja Utama" - bahasakita.co.id
 * All Rights Reserved.
 ******************************************************************************/
@Database(
        entities = {
                LoggingKrEntity.class
//                UtteranceEntity.class,
//                MetatranscriptEntity.class
        },
        version = 1,
        exportSchema = false)
@TypeConverters({DateTimeConverter.class})
public abstract class AppDatabase extends RoomDatabase {
//    private static AppDatabase INSTANCE;

        public abstract LoggingKrDao loggingKr();
//    public abstract UtteranceDao utteranceDao();
//    public abstract MetatranscriptDao metatranscriptDao();

//    public static AppDatabase getInstance(Context context) {
//        if (INSTANCE == null) {
//            synchronized (AppDatabase.class) {
//                if (INSTANCE == null) {
//                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
//                            AppDatabase.class, "Kutulis.db")
////                            .fallbackToDestructiveMigration()
//                            .build();
//                    Log.i("Database", context.getDatabasePath("Kutulis.db").getPath());
//                }
//            }
//        }
//        return INSTANCE;
//    }
}
